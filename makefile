CC=g++

CFLAGS=-c -Wall

all: mylinks

mylinks: mylinks.o
        $(CC) mylinks.o -o mylinks

mylinks.o: mylinks.cc
        $(CC) $(CFLAGS) mylinks.cc

clean:
        rm -rf *.o
        rm -rf mylinks