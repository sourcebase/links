#ifndef __MYLINKS_HH__
#define __MYLINKS_HH__ 
#include<iostream>
using namespace std ;
struct node{
	int data ;
	struct node *next ;
}*head;
class mylinks{
public :
	mylinks(){
		head = NULL ;
	}
	~mylinks(){
	}
	bool addToLinksToLast(int);
	bool displayLinks();
	bool addToLinksToFirst(int);
	bool addToLinksAtIndex(int ,int);
	bool deleteLinksAtIndex();
	bool getCountofData(int);
	bool getLengthOfList(int * );
	bool getDataAtIndex(int *, int * );
	bool deleteMyList();
	bool insertAsSorted(int);
	bool sortingList();
}; 
bool mylinks::deleteMyList()
{
        cout << "deleting the list "<< endl ;
	if(head == NULL){
		cout << "list is already empty" << endl ;
		return true ;
	}
	else{
		cout << "deleting "<< endl ;
		node *current = head ;
		node *next = new node ;
		while(current != NULL){
			next = current->next ;
			delete(current);
			cout << "deleted lptr "<< endl ;
			current = next ;
		}
		delete current ;
		delete head ;
	}
        return true ;
}
bool mylinks::deleteLinksAtIndex()
{
	cout << "deleting the list "<< endl ;
	node *temp = new node ;
	temp = head ;
	int count = 1 ;
	while(temp->next != NULL)
	{
		temp = temp->next ;
		count++;
	}
	return true ;
}
bool mylinks::addToLinksAtIndex(int val ,int index){
                cout << __func__ <<"Data will be inserted after index "<< index <<endl ;
                node *lptr = new node ;
                lptr = head ;
                int count = 1 ;
                while(lptr->next != NULL)
                {
                                if(count == index)
                                {
                                                node *temp = new node ;
                                                temp->data = val ;
                                                temp->next = lptr->next;
                                                lptr->next = temp ;
                                                return true;
                                }
                count++ ;
                lptr = lptr->next ;
                }
                return true ;
}
bool mylinks::addToLinksToFirst(int val){
                if(head == NULL){
                                head = new node ;
                head->data = val ;
                head->next = NULL ;
                }
                else{
                node *temp = new node ;
                temp->data = val ;
                temp->next = head ;
                head = temp ;
                }
                return true ;
}
bool mylinks::addToLinksToLast(int val){
                if(head == NULL){
                                head = new node ;
                                head->data = val ;
                                head->next = NULL ;
                }
                else{
                                node *lptr = new node ;
                                node *temp = new node ;
                                temp = head ;
                                lptr->data = val ;
                                lptr->next = NULL ;
                                while(temp->next != NULL)
                                {
                                                temp = temp->next;
                                }
                                temp->next = lptr ;
                }
                return true ;
}
bool mylinks::getLengthOfList(int *Totalcount){
	*Totalcount = 0 ;
	int count = 0 ;
	if(head == NULL){
		cout << "empty list" << endl ;
		return true ;
	}
	else{
		node *lptr = new node ;
		lptr = head ;
		while(lptr->next != NULL){
			count++;
			lptr = lptr->next ;
		}
		count++;		
	}
	*Totalcount = count ;
	return true ;
}
bool mylinks::getCountofData(int search)
{
	cout << "getCount of the number "<< search << endl ;
	int count = 0;
        if(head == NULL){
                cout << "empty list " << endl ;
		return true ;
	}
        else{
		node *lptr = new node ;
		lptr = head ;
		while (lptr->next != NULL )
		{
			if(lptr->data == search)
				count++ ;
			lptr = lptr->next ;
		}
		if(lptr->data == search)
		count++ ;
        }
	cout << search <<" found " << count << " times "<<endl ;
	return true ;
}
bool mylinks::getDataAtIndex(int *data ,int *index){
	int totalCount ;
	if(getLengthOfList(&totalCount))
	{
		cout << "totalCount " << totalCount  << endl ;
		if(totalCount < *index){
			cout << "Asking for index" << *index << " hence returning "<< endl ;
			return true ;
		}
	}
	if(head == NULL){
		cout << "linked list is empty" << endl ;
		return true ;
	}
	else{
	        node *lptr = new node ;
	        lptr = head ;
		int count = 0 ;
		while(lptr->next != NULL ){
			if(*index == count){
				cout << " count "<< count << " index " << *index << " Value is  " << lptr->data << endl ;
				*data =lptr->data ;
				return true ;
			}
			lptr = lptr->next ;
			count++ ;
		}
		cout << "Last Value :: count "<< count << "index " << *index << "Value is  " << lptr->data << endl ;
		*data = lptr->data ;
	}
	return true ;
}
bool mylinks::insertAsSorted(int ldata){
	node *newnode = new node ;
	newnode->data = ldata ;
	newnode->next = NULL ;
	if(head == NULL)	
		return true ;
	else{
	node *lptr = new node ;
	lptr = head ;
	while(lptr->next != NULL && lptr->data > newnode->data){
		lptr = lptr->next ;
		}
                newnode->next = lptr->next ;
                lptr->next = newnode ;
		
	}
	return true ;	
}
bool mylinks::sortingList(){
//        node *newnode = new node ;
//        newnode->data = ldata ;
//        newnode->next = NULL ;
        if(head == NULL)
                return true ;
        else{
        node *lptr = new node ;
        lptr = head ;
        while(lptr->next != NULL || lptr->data < lptr->next->data){
		cout << "found" << endl ;
		cout << "lptr->data " << lptr->data << endl ;
		cout << "lptr->next->data " << lptr->next->data << endl ;
                lptr = lptr->next ;
                }
/              newnode->next = lptr->next ;
    //            lptr->next = newnode ;

        }
        return true ;
}
bool mylinks::displayLinks(){
                cout << "Linked list ::: " << endl ;
                if(head == NULL)
                                cout << "link list is empty" << endl ;
                else{
                                node *lptr = new node ;
                                lptr = head ;
                                while(lptr->next != NULL){
						cout <<lptr->data << "->";
                                                lptr = lptr->next ;
                                }
				cout <<lptr->data << endl ;
                }
                return true ;
}
#endif
